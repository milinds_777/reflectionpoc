package com.mindstix.service;

public interface Area {
	Double getArea(Double param);
}
