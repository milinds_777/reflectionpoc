package com.mindstix.service.impl;

import com.mindstix.service.Area;

public class Square implements Area {
	@Override
	public Double getArea(Double param) {
		return param * param;
	}
}