package com.mindstix.service.impl;

import com.mindstix.service.Area;

public class Sphere implements Area {

	@Override
	public Double getArea(Double param) {
		return 4 * Math.PI * Math.pow(param, 3);
	}

}