package com.mindstix.service.impl;

import com.mindstix.service.Area;

public class Circle implements Area {

	@Override
	public Double getArea(Double param) {
		return Math.PI * Math.pow(param, 2) * 2;
	}

}