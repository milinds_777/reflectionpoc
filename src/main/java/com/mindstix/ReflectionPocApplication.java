package com.mindstix;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

//@SpringBootApplication
public class ReflectionPocApplication {
	
	@Autowired
	private Environment environment;

	public static void main(String[] args) throws ClassNotFoundException, FileNotFoundException, IOException, URISyntaxException {
		SpringApplication.run(ReflectionPocApplication.class, args);
		// fqcnl: fully qualified class names list
		Optional<List<String>> fqcnl = Optional.empty();
		List<Class<?>> clsList = new ArrayList<>();
		ReflectionPocApplication reflectionPocApplication = new ReflectionPocApplication();
		try {
			fqcnl = Optional.of(reflectionPocApplication.getProps("bootstrap.properties"));
			for (String itr : fqcnl.get()) {
				clsList.add(Class.forName(itr));
			}
		} catch (FileNotFoundException e) {
			System.err.println("--FileNotFoundException--");
		} catch (IOException e) {
			System.err.println("--IOException--");
		}
		clsList.stream().map(itr->itr.getDeclaredMethods()).collect(toList());
		List<Class<?>[]> implementedInterfaces = clsList.stream().map(itr->itr.getInterfaces()).collect(toList());
		List<Method> interfaceMethods = new ArrayList<>();
		for (Class<?>[] classes : implementedInterfaces) {
			for (Class<?> cls : classes) {
				interfaceMethods.addAll(Arrays.asList(cls.getDeclaredMethods()));
			}
		}
		interfaceMethods.stream().distinct().forEach(System.out::println);
		// Method[] areaDeclaredMethods = Class.forName("com.mindstix.service.Area").getDeclaredMethods();
		// List<String> areaMethods = Arrays.stream(areaDeclaredMethods).map(x -> x.getName()).collect(toList());
		// List<String> circleMethods = clsList.stream().map(x -> x.getName()).collect(toList());
		// List<String> commonInterfaceMethods = circleMethods.stream()
		// .flatMap(x -> areaMethods.stream().filter(y ->
		// x.equals(y))).collect(toList());
		// commonInterfaceMethods.stream().forEach(System.out::println);
		// System.out.println("-------------------------------------------------------");
		// areaMethods.stream().forEach(System.out::println);
		// circleMethods.stream().forEach(System.out::println);
		// Class<?>[] circleInterfaces =
		// Class.forName("com.mindstix.service.impl.Circle").getInterfaces();
		// clsList.stream().map(c->c.getDeclaredMethods());
		// System.out.println(areaDeclaredMethods[0].getName());
		// clsList.stream().map(c->c.getMethod("",
		// Class.forName("com.mindstix.service.Area").getDeclaredMethods()))
		// Arrays.stream(areaDeclaredMethods).forEach(System.out::println);
		// Arrays.stream(circleDeclaredMethods).map(x->x.).forEach(System.out::println);
		// Arrays.stream(circleInterfaces).forEach(System.out::println);
	}

	/**
	 * Get the list of properties for a given key.
	 * 
	 * @param fileName
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private List<String> getProps(String fileName) throws FileNotFoundException, IOException, URISyntaxException {

		// Properties properties = new Properties();
		// properties.load(new FileInputStream(fileName));
		// for (Enumeration<?> e = properties.propertyNames(); e.hasMoreElements();) {
		// String name = (String) e.nextElement();
		// System.out.println(name);
		// String value = properties.getProperty(name);
		// System.out.println(value);
		// }

//		 System.out.println("-------------------------------------------------------");
		if(StringUtils.isEmpty(fileName)) {
			fileName = "application.properties";
		}
		Properties props = new Properties();
		String file = new File(this.getClass().getClassLoader().getResource(fileName).toURI()).getPath();
		props.load(new FileInputStream(file));
		List<String> propertyValues = new ArrayList<String>();
		for (Enumeration<?> e = props.propertyNames(); e.hasMoreElements();) {
			String name = (String) e.nextElement();
			String value = props.getProperty(name);
			// System.out.println(String.format("name: [%s], value: [%s]", name, value));
			if (name.startsWith("class.")) {
				propertyValues.add(value);
			}
		}
		String[] activeProfiles = environment.getActiveProfiles();
		Arrays.toString(activeProfiles);
		// Enumeration<?> propertyKeys = props.propertyNames();
		// while (propertyKeys.hasMoreElements()) {
		// String param = (String) propertyKeys.nextElement();
		// propertyValues.add(props.getProperty(param));
		// }
//		 propertyValues.stream().forEach(System.out::println);
//		 System.out.println("-------------------------------------------------------");
		return propertyValues;
	}
}
